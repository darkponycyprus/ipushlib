# Android repository for the ipush SDK
### Android Studio integration

---

### In your build.gradle app module

* add 

```
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

### In your build.gradle app module

* add the below code on top

```
buildscript {
	repositories {
		maven { url "https://plugins.gradle.org/m2/" }
	}
	dependencies {
		classpath "gradle.plugin.com.onesignal:onesignal-gradle-plugin:[0.11.0, 0.99.99]"
	}
}
apply plugin: "com.onesignal.androidsdk.onesignal-gradle-plugin"
```

* in android defaultConfig add (APP_ID is provided)

```
manifestPlaceholders = [
    onesignal_app_id               : "{APP_ID}",  
    onesignal_google_project_number: "REMOTE"
]
```	

* add the library in dependencies

```
implementation "org.bitbucket.darkponycyprus:ipushlib:0.0.{LATEST_VERSION}"
```	

* example of a complete file

```
buildscript {
        repositories {
            maven { url "https://plugins.gradle.org/m2/" }
        }
        dependencies {
            classpath "gradle.plugin.com.onesignal:onesignal-gradle-plugin:[0.11.0, 0.99.99]"
        }
}

apply plugin: "com.onesignal.androidsdk.onesignal-gradle-plugin"

apply plugin: "com.android.application"

android {
    compileSdkVersion 28
    defaultConfig {
        applicationId "eu.darkpony.myapplication"
        minSdkVersion 16
        targetSdkVersion 28
        versionCode 1
        versionName "1.0"
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
        manifestPlaceholders = [
                onesignal_app_id               : "{APP_ID}",
                onesignal_google_project_number: "REMOTE"
        ]
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro"
        }
    }
}

dependencies {
    implementation fileTree(dir: "libs", include: ["*.jar"])
    implementation "com.android.support:appcompat-v7:28.0.0"
    implementation "com.android.support.constraint:constraint-layout:1.1.3"
    implementation "org.bitbucket.darkponycyprus:ipushlib:0.0.{LATEST_VERSION}"
    testImplementation "junit:junit:4.12"
    androidTestImplementation "com.android.support.test:runner:1.0.2"
    androidTestImplementation "com.android.support.test.espresso:espresso-core:3.0.2"
}
```

### Resources

* Add strings provided

```
<string name="base_url">http://ipush.test/</string>
<string name="api_url">http://ipush.test/api/</string>
```

### Manifest
* Inside of application tag add

```
<meta-data
    android:name="com.onesignal.NotificationOpened.DEFAULT"
    android:value="DISABLE" />
            
```

### Create your application class

* In your manifest add android:name=".ApplicationClass" to your <application> tag. Also add android:usesCleartextTraffic="true"

```
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.example.test_login_activity">
   <application
        android:icon="@mipmap/ic_launcher"
        android:usesCleartextTraffic="true"
        android:label="@string/app_name"
        android:name=".ApplicationClass">
        ...
    </application>
</manifest>
```

* Hover over android:name=".ApplicationClass" or press Alt + Enter or Option + Enter and select Create class 'ApplicationClass'

* Extend IpushApplication class and initialize. In the function initIpush the second argument is the Main Activity of the application, the activity that is showing on start

```java
import eu.darkpony.ipushlib.IpushApplication;

public class ApplicationClass extends IpushApplication {
   @Override
   public void onCreate() {
       super.onCreate();

       initIpush(getString(R.string.api_url), MainActivity.class);
   }
}
```

### In your main activity

* Inside onCreate() add

```
IpushApplication.handleNotificationOpen(getIntent(), this);
```

* Also add onStart(), onStop()

```
@Override
protected void onStart() {
    super.onStart();

    ApplicationClass.getIpushApiInstance().sessionOpened(null);
}

@Override
protected void onStop() {
    super.onStop();

    ApplicationClass.getIpushApiInstance().sessionClosed(null);
}
```

* Example

```java
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IpushApplication.handleNotificationOpen(getIntent(), this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    
        ApplicationClass.getIpushApiInstance().sessionOpened(null);
    }
    
    @Override
    protected void onStop() {
        super.onStop();
    
        ApplicationClass.getIpushApiInstance().sessionClosed(null);
    }
}
```

### Api calls

* campaigns(IpushTaskListener taskListener): Get campaigns for a user(based on the uid that is stored in th device)
* groups(IpushTaskListener taskListener): Get user groups
* notificationTapped(String campaignId, IpushTaskListener taskListener): User clicked notification
* richMediaBtnTapped(String campaignId, IpushTaskListener taskListener): User clicked rich media template call to action button
* sessionClosed(IpushTaskListener taskListener): Application is closed
* sessionOpened(IpushTaskListener taskListener): Application is active
* setPlayerGroups(JSONArray groupIdsJson, IpushTaskListener taskListener): Save user's groups selection
* All the api calls are async requests. If you want to implement some functionality when the request has returned a response use a taskListener, for example:
```
IpushTaskListener<JSONObject> postTaskListener = new IpushTaskListener<JSONObject>() {
    @Override
    public void onPostTask(JSONObject result) {
        //the request was completed
    }
};
ApplicationClass.getIpushApiInstance().sessionOpened(postTaskListener);
```