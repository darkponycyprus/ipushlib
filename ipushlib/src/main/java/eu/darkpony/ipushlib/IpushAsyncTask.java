package eu.darkpony.ipushlib;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

public class IpushAsyncTask extends AsyncTask {
    String route, method, timestamp, token;
    JSONObject postData;
    IpushTaskListener postTaskListener;

    public IpushAsyncTask(String method, String route, String timestamp, String token, JSONObject postData, IpushTaskListener postTaskListener){
        this.timestamp = timestamp;
        this.token = token;
        this.method = method;
        this.route = route;
        this.postData = postData;
        this.postTaskListener = postTaskListener;
    }

    @Override
    protected void onPreExecute() {
        this.route += "?timestamp="+this.timestamp+"&hashed_timestamp="+this.token;

        if(postData != null && this.method.equals("GET") && this.postData.length() > 0){
            Iterator<String> iter = this.postData.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    Object value = this.postData.get(key);
                    this.route += "&"+key+"="+value;
                } catch (JSONException e) {

                }
            }
        }

        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        StringBuilder result = new StringBuilder();
        HttpURLConnection connection = null;

        try {
            Log.d("ipush_log", "url: "+this.route);
            //Open a new URL connection
            URL url = new URL(this.route);
            connection = (HttpURLConnection) url.openConnection();

            //Defines a HTTP request type
            connection.setRequestMethod(this.method);

            //Sets headers: Content-Type, Authorization
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");

            //Create a writer object and make the request
            if(postData != null && this.method.equals("POST")) {
                Log.d("ipush_log", "api request with post data: "+this.postData.toString());
                OutputStreamWriter outputStream = new OutputStreamWriter(connection.getOutputStream());
                outputStream.write(this.postData.toString());
                outputStream.flush();
                outputStream.close();
            }

            InputStream in = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            //Get the Response code for the request
            Log.d("ipush_log", "code: "+connection.getResponseCode());
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            return result.toString();
        } catch (Exception e) {
            Log.d("ipush_log", "Exception: "+e.toString());
            e.printStackTrace();
        }finally {
            if(connection != null) {
                connection.disconnect();
            }
        }

        return -1;
    }

    @Override
    protected void onPostExecute(Object result) {
        if (result != null && postTaskListener != null) {
            try {
                postTaskListener.onPostTask(new JSONObject(result.toString()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.onPostExecute(result);
    }

    //Helpers

    /*
     * Init action api post body data
     */
//    private static JSONObject initPostObject(String campaignId, String action){
//        JSONObject postData = new JSONObject();
//
//        try {
//            postData.put("uid", getUid());
//            postData.put("campaign_id", campaignId);
//            postData.put("type", action);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return postData;
//    }
}
