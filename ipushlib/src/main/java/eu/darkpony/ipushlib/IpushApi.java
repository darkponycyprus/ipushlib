package eu.darkpony.ipushlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;

import eu.darkpony.ipushlib.helpers.IpushHelper;

public class IpushApi{
    String baseUrl;

    //api tokens
    public static String timestamp = "", token = "";

    public IpushApi(String baseUrl){
        this.baseUrl = baseUrl;
        Long tsLong = System.currentTimeMillis()/1000;
        timestamp = tsLong.toString();
        try {
            token = IpushHelper.md5("secret"+timestamp);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void campaigns(IpushTaskListener postTaskListener){
        String method = "GET";
        JSONObject getData = new JSONObject();
        try {
            getData.put("uid", IpushHelper.getOneSignalUid());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new IpushAsyncTask(method, this.baseUrl+"campaigns", timestamp, token, getData, postTaskListener).execute();
    }

    public void groups(String uid, IpushTaskListener postTaskListener){
        String method = "GET";
        JSONObject postData = null;

        String url = this.baseUrl+"groups";
        if(uid != null){
            url += "/" + uid;
        }
        new IpushAsyncTask(method, url, timestamp, token, postData, postTaskListener).execute();
    }

    public void notificationTapped(String campaignId, IpushTaskListener postTaskListener){
        String method = "POST";
        JSONObject postData = new JSONObject();

        try {
            postData.put("uid", IpushHelper.getOneSignalUid());
            postData.put("campaign_id", campaignId);
            postData.put("type", "tapped_notification");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(postData.has("uid") && postData.has("campaign_id") && postData.has("type")) {
            new IpushAsyncTask(method, this.baseUrl+"action", timestamp, token, postData, postTaskListener).execute();
        }
    }

    public void richMediaBtnTapped(String campaignId, IpushTaskListener postTaskListener){
        String method = "POST";
        JSONObject postData = new JSONObject();

        try {
            postData.put("uid", IpushHelper.getOneSignalUid());
            postData.put("campaign_id", campaignId);
            postData.put("type", "tapped_rich_media_button");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(postData.has("uid") && postData.has("campaign_id") && postData.has("type")) {
            new IpushAsyncTask(method, this.baseUrl+"action", timestamp, token, postData, postTaskListener).execute();
        }
    }

    public void sendCustomAction(String action, IpushTaskListener postTaskListener){
        String method = "POST";
        JSONObject postData = new JSONObject();

        try {
            postData.put("uid", IpushHelper.getOneSignalUid());
            postData.put("action", action);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(postData.has("uid") && postData.has("action")) {
            new IpushAsyncTask(method, this.baseUrl+"custom-action", timestamp, token, postData, postTaskListener).execute();
        }
    }

    public void sessionClosed(IpushTaskListener postTaskListener){
        String method = "POST";
        JSONObject postData = new JSONObject();

        try {
            postData.put("uid", IpushHelper.getOneSignalUid());
            postData.put("type", "closed");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(postData.has("uid") && postData.has("type")) {
            new IpushAsyncTask(method, this.baseUrl+"session", timestamp, token, postData, postTaskListener).execute();
        }
    }

    public void sessionOpened(IpushTaskListener postTaskListener){
        String method = "POST";
        JSONObject postData = new JSONObject();

        try {
            postData.put("uid", IpushHelper.getOneSignalUid());
            postData.put("type", "opened");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(postData.has("uid") && postData.has("type")) {
            new IpushAsyncTask(method, this.baseUrl+"session", timestamp, token, postData, postTaskListener).execute();
        }
    }

    /*
    Refresh lat long for a uid
    */
    public void setPlayerCoordinates(double lat, double lon, IpushTaskListener postTaskListener){
        String method = "POST";
        JSONObject postData = new JSONObject();

        try {
            postData.put("uid", IpushHelper.getOneSignalUid());
            if(lat > 0){postData.put("lat", lat);}
            if(lon > 0){postData.put("lon", lon);}
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(postData.has("uid") && postData.has("lat") && postData.has("lon")) {
            new IpushAsyncTask(method, this.baseUrl+"player-coordinates", timestamp, token, postData, postTaskListener).execute();
        }
    }

    /*
    Set groups for a uid
     */
    public void setPlayerGroups(JSONArray groupIdsJson, IpushTaskListener postTaskListener){
        String method = "POST";
        JSONObject postData = new JSONObject();

        try {
            postData.put("player_uid", IpushHelper.getOneSignalUid());
            postData.put("groups_ids", groupIdsJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(postData.has("player_uid") && postData.has("groups_ids")) {
            new IpushAsyncTask(method, this.baseUrl+"user-set-group", timestamp, token, postData, postTaskListener).execute();
        }
    }

    /*
    Refresh profile for a uid
    */
    public void updatePlayerProfile(JSONObject postData, IpushTaskListener postTaskListener){
        String method = "POST";

        try {
            postData.put("uid", IpushHelper.getOneSignalUid());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(postData != null && postData.length() > 1) {
            Log.d("ipush_log", "updatePlayerProfile: "+postData.toString());
            new IpushAsyncTask(method, this.baseUrl+"player", timestamp, token, postData, postTaskListener).execute();
        }
    }
}
