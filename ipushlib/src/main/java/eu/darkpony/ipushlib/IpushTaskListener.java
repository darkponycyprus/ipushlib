package eu.darkpony.ipushlib;

public interface IpushTaskListener<JSONObject> {
    void onPostTask(JSONObject result);
}
