package eu.darkpony.ipushlib.helpers;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import eu.darkpony.ipushlib.IpushApi;
import eu.darkpony.ipushlib.IpushApplication;

public class IpushHelper {

    /*
     * Get uid of user
     */
    public static String getOneSignalUid(){
        return OneSignal.getPermissionSubscriptionState().getSubscriptionStatus().getUserId();
    }

    public static boolean isPlayerSubscribed(){
        if(OneSignal.getPermissionSubscriptionState().getSubscriptionStatus().getUserSubscriptionSetting()){
            return true;
        }else{
            return false;
        }
    }

    //return md5 of a string
    public static String md5(String input) throws NoSuchAlgorithmException {
        String result = input;
        if(input != null) {
            MessageDigest md = MessageDigest.getInstance("MD5"); //or "SHA-1"
            md.update(input.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            result = hash.toString(16);
            while(result.length() < 32) { //40 for SHA-1
                result = "0" + result;
            }
        }
        return result;
    }

    public static void setPlayerSubscribed(boolean subscribed){
        if(subscribed){
            OneSignal.setSubscription(true);
        }else{
            OneSignal.setSubscription(false);
        }
    }

    public static void updatePlayerInfo(Context context){
        //get country from sim
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String simCountry = tm.getSimCountryIso();

        if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
            simCountry = simCountry.toUpperCase(Locale.US);
        }
        else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
            String networkCountry = tm.getNetworkCountryIso();
            if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                simCountry = networkCountry.toUpperCase(Locale.US);
            }
        }

        if (simCountry != null && simCountry.length() == 2) {
            //create post data object
            JSONObject postData = new JSONObject();

            try {
                postData.put("country_code", simCountry);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //call api to save data
            IpushApplication.getIpushApiInstance().updatePlayerProfile(postData, null);
        }
    }
}
