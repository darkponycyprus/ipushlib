package eu.darkpony.ipushlib;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class IpushApplication extends Application{
    public static Class launcherActivityClass;
    private static IpushApi ipushapi = null;
    private Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        if(context == null){
            context = getApplicationContext();
        }

        initOneSignal();
    }

    // IpushHelper Functions

    public static IpushApi getIpushApiInstance(){
        return ipushapi;
    }

    /*
     * Get launcher intent
     */
    private Intent getLauncherActivityIntent(){
        final PackageManager pm = getPackageManager();
        Intent intent = pm.getLaunchIntentForPackage(getPackageName());

        return intent;
    }

    /*
     * Extract notification data and open default launcher activity
     */
    private void handleNotificationClick(JSONObject data){
        String action, id, extUrl, richMediaTemplateUrl, customDeeplink;

        if(data != null) {
            id = data.optString("id", null);
            action = data.optString("action", null);
            extUrl = data.optString("ext_url", null);
            richMediaTemplateUrl = data.optString("richmediatemplate", null);
            customDeeplink = data.optString("custom_deeplink", null);

            if(id != null){
                getIpushApiInstance().notificationTapped(id, null);
            }
            Log.d("ipush_log", "handleNotificationClick action: "+action);


            //start new activity and pass data
            if (action != null && id != null) {
                Log.d("ipush_log", "handleNotificationClick action: "+action);
                //Intent intent = getLauncherActivityIntent();
                Intent intent = new Intent(this, launcherActivityClass);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id", id);
                intent.putExtra("action", action);
                if (richMediaTemplateUrl != null) {
                    Log.d("ipush_log", "richMediaTemplateUrl: "+richMediaTemplateUrl);
                    intent.putExtra("richMediaTemplateUrl", richMediaTemplateUrl);
                }
                if (extUrl != null) {
                    Log.d("ipush_log", "extUrl: "+extUrl);
                    intent.putExtra("extUrl", extUrl);
                }
                if (customDeeplink != null) {
                    Log.d("ipush_log", "customDeeplink: "+customDeeplink);
                    intent.putExtra("custom_deeplink", customDeeplink);
                }
                Log.d("ipush_log", "Starting launcher activity");
                startActivity(intent);
            }
        }
    }

    /*
     * Handle notification tap
     */
    public static void handleNotificationOpen(Intent intent, Context context){
        //Log.d("ipush_log", "handleNotificationOpen");
        if(intent.hasExtra("action") && intent.hasExtra("id")) {
            if(intent.hasExtra("extUrl")) {
                String extUrl = intent.getStringExtra("extUrl");
                if(!extUrl.contains("http")) extUrl = "http://" + extUrl;
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(extUrl)));
            }

            if(intent.hasExtra("richMediaTemplateUrl")) {
                Intent intentRichMedia = new Intent(context, IpushWebview.class);
                intentRichMedia.putExtra("richMediaTemplateUrl", intent.getStringExtra("richMediaTemplateUrl"));
                intentRichMedia.putExtra("campaignId", intent.getStringExtra("id"));
                context.startActivity(intentRichMedia);
            }
        }
    }

    /*
     * Initialize one signal
     */
    private void initOneSignal(){
        OneSignal.startInit(context)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(new NotificationOpenedHandler())
                .setNotificationReceivedHandler(new NotificationReceivedHandler())
                .init();
    }

    public static void initIpush(String baseUrl, Class launcherActivityClass){
        IpushApplication.launcherActivityClass = launcherActivityClass;
        if(ipushapi == null){
            ipushapi = new IpushApi(baseUrl);
        }
    }

    //Classes

    private class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            JSONObject data = result.notification.payload.additionalData;

            if(data != null) {
                handleNotificationClick(data);
            }
        }
    }

    private class NotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {

        }
    }
}
