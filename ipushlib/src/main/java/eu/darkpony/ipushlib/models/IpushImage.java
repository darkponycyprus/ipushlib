package eu.darkpony.ipushlib.models;

public class IpushImage {
    private int id;
    private String originalName, name, path, ext;

    public IpushImage(int id, String originalName, String name, String path, String ext){
        this.id = id;
        this.originalName = originalName;
        this.name = name;
        this.path = path;
        this.ext = ext;
    }

    public int getId() {
        return id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public String getExt() {
        return ext;
    }

    public String getWebUrl(String baseUrl){
        return baseUrl + path + "/" + name + "." + ext;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}
