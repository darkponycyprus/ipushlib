package eu.darkpony.ipushlib.models;

public class IpushGroup {
    private int id;
    private String name;
    private boolean checked;

    public IpushGroup(int id, String name, boolean checked){
        this.id = id;
        this.name = name;
        this.checked = checked;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
