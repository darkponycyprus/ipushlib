package eu.darkpony.ipushlib.models;

public class IpushCampaign {
    private int id, status;
    private String action, contentTitle, contentDescription, completedAt, externalWebUrl, startDate;
    private IpushImage banner, icon;
    private IpushTemplate template;

    public IpushCampaign(int id, int status, String action, String contentTitle, String contentDescription, String completedAt, String externalWebUrl, String startDate){
        this.id = id;
        this.status = status;
        this.action = action;
        this.contentTitle = contentTitle;
        this.contentDescription = contentDescription;
        this.completedAt = completedAt;
        this.externalWebUrl = externalWebUrl;
        this.startDate = startDate;
    }

    public int getId() {
        return id;
    }

    public int getStatus() {
        return status;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public String getContentDescription() {
        return contentDescription;
    }

    public String getAction() {
        return action;
    }

    public String getCompletedAt() {
        return completedAt;
    }

    public String getExternalWebUrl() {
        return externalWebUrl;
    }

    public String getStartDate() {
        return startDate;
    }

    public IpushImage getBanner() {
        return banner;
    }

    public IpushImage getIcon() {
        return icon;
    }

    public IpushTemplate getTemplate() {
        return template;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public void setContentDescription(String contentDescription) {
        this.contentDescription = contentDescription;
    }

    public void setCompletedAt(String completedAt) {
        this.completedAt = completedAt;
    }

    public void setExternalWebUrl(String externalWebUrl) {
        this.externalWebUrl = externalWebUrl;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setBanner(IpushImage banner) {
        this.banner = banner;
    }

    public void setIcon(IpushImage icon) {
        this.icon = icon;
    }

    public void setTemplate(IpushTemplate template) {
        this.template = template;
    }
}
