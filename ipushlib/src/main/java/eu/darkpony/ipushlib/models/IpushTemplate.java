package eu.darkpony.ipushlib.models;

public class IpushTemplate {
    private int id;
    private String type, name;

    public IpushTemplate(int id, String type, String name){
        this.id = id;
        this.type = type;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getWebUrl(String baseUrl){
        return baseUrl + "templates/" + id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }
}
