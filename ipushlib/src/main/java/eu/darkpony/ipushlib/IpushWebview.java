package eu.darkpony.ipushlib;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class IpushWebview extends AppCompatActivity {
    WebView webView;
    ImageButton closeRichMediaIb;
    Context ctx;
    LinearLayout webViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_ipush_web_view);
        ctx = this;

        webView = (WebView) findViewById(R.id.ipushrichweb);
        closeRichMediaIb = (ImageButton) findViewById(R.id.close_rich_media_ib);
        webViewContainer = (LinearLayout) findViewById(R.id.webviewcontainer);

        this.setFinishOnTouchOutside(true);
        Display display = getWindowManager(). getDefaultDisplay();
        Point size = new Point();
        display. getSize(size);
        int width = size. x;
        int height = size. y;
        LinearLayout.LayoutParams paramsWV = new LinearLayout.LayoutParams(width - 150, height - 300);
        webViewContainer.setLayoutParams(paramsWV);

        Intent intent = getIntent();
        if(intent.hasExtra("richMediaTemplateUrl")){
            webView.setWebViewClient(new IpushWebViewClient(intent.getStringExtra("campaignId")));
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl(intent.getStringExtra("richMediaTemplateUrl"));
        }

        closeRichMediaIb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
    }

    /*
     * WebViewClient class
     */

    private class IpushWebViewClient extends WebViewClient{
        private String campaignId;

        public IpushWebViewClient(String campaignId){
            this.campaignId = campaignId;
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            IpushApplication.getIpushApiInstance().richMediaBtnTapped(this.campaignId, null);

            view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            return true;
        }
    }
}
