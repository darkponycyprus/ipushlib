package eu.darkpony.ipush;

import android.app.Application;

import eu.darkpony.ipushlib.IpushApplication;

public class ApplicationClass extends IpushApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        initIpushApi(getString(R.string.api_url));
    }
}
