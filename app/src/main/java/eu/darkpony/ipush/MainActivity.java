package eu.darkpony.ipush;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.onesignal.OneSignal;

import org.json.JSONObject;

import eu.darkpony.ipushlib.IpushApplication;
import eu.darkpony.ipushlib.IpushTaskListener;

public class MainActivity extends AppCompatActivity {
    TextView uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        uid = (TextView) findViewById(R.id.uidtv);
        uid.setText(OneSignal.getPermissionSubscriptionState().getSubscriptionStatus().getUserId());

        IpushApplication.handleNotificationOpen(getIntent(), this);
    }

    @Override
    protected void onStart() {
        super.onStart();

//        IpushTaskListener<JSONObject> postTaskListener = new IpushTaskListener<JSONObject>() {
//            @Override
//            public void onPostTask(JSONObject result) {
//                Log.d("ipush_log", result.toString());
//            }
//        };
//        ApplicationClass.getIpushApiInstance().sessionOpened(postTaskListener);
        IpushTaskListener<JSONObject> postTaskListener = new IpushTaskListener<JSONObject>() {
            @Override
            public void onPostTask(JSONObject result) {
                Log.d("ipush_log", result.toString());
            }
        };
        ApplicationClass.getIpushApiInstance().groups(postTaskListener);
    }

    @Override
    protected void onStop() {
        super.onStop();

        ApplicationClass.getIpushApiInstance().sessionClosed(null);
    }

}
